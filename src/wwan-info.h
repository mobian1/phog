/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>
#include "status-icon.h"

G_BEGIN_DECLS

#define PHOG_TYPE_WWAN_INFO (phog_wwan_info_get_type())

G_DECLARE_FINAL_TYPE (PhogWWanInfo, phog_wwan_info, PHOG, WWAN_INFO, PhogStatusIcon)

GtkWidget * phog_wwan_info_new (void);
void        phog_wwan_info_set_show_detail (PhogWWanInfo *self, gboolean show);
gboolean    phog_wwan_info_get_show_detail (PhogWWanInfo *self);

G_END_DECLS
