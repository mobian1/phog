/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define PHOG_TYPE_OSK_MANAGER (phog_osk_manager_get_type())

G_DECLARE_FINAL_TYPE (PhogOskManager, phog_osk_manager, PHOG, OSK_MANAGER, GObject)

PhogOskManager * phog_osk_manager_new           (void);
gboolean          phog_osk_manager_get_available (PhogOskManager *self);
void              phog_osk_manager_set_visible   (PhogOskManager *self, gboolean visible);
gboolean          phog_osk_manager_get_visible   (PhogOskManager *self);

G_END_DECLS
