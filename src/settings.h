/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

#define PHOG_TYPE_SETTINGS (phog_settings_get_type())

G_DECLARE_FINAL_TYPE (PhogSettings, phog_settings, PHOG, SETTINGS, GtkBin)

GtkWidget * phog_settings_new (void);
gint        phog_settings_get_drag_handle_offset (PhogSettings *self);
