/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "lockscreen.h"
#include <gtk/gtk.h>

#define PHOG_TYPE_LOCKSCREEN_MANAGER (phog_lockscreen_manager_get_type())

G_DECLARE_FINAL_TYPE (PhogLockscreenManager,
                      phog_lockscreen_manager,
                      PHOG,
                      LOCKSCREEN_MANAGER,
                      GObject)

PhogLockscreenManager *phog_lockscreen_manager_new (void);
void                    phog_lockscreen_manager_set_locked  (PhogLockscreenManager *self,
                                                              gboolean state);
gboolean                phog_lockscreen_manager_get_locked  (PhogLockscreenManager *self);
gboolean                phog_lockscreen_manager_set_page  (PhogLockscreenManager *self,
                                                            PhogLockscreenPage     page);
PhogLockscreenPage     phog_lockscreen_manager_get_page  (PhogLockscreenManager *self);
void                    phog_lockscreen_manager_set_timeout (PhogLockscreenManager *self,
                                                              int timeout);
int                     phog_lockscreen_manager_get_timeout (PhogLockscreenManager *self);
gint64                  phog_lockscreen_manager_get_active_time (PhogLockscreenManager *self);
