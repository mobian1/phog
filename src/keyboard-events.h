/*
 * Copyright (C) 2020 Evangelos Ribeiro Tzaras
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Evangelos Ribeiro Tzaras <devrtz@fortysixandtwo.eu>
 */

#pragma once

#include <stdint.h>
#include <gtk/gtk.h>

#define PHOG_TYPE_KEYBOARD_EVENTS (phog_keyboard_events_get_type ())

G_DECLARE_FINAL_TYPE (PhogKeyboardEvents,
                      phog_keyboard_events,
                      PHOG,
                      KEYBOARD_EVENTS,
                      GSimpleActionGroup)

PhogKeyboardEvents  *phog_keyboard_events_new           (void);
